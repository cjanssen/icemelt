function initCamera()
	camera = {
		pos = Vector(0,0),
		dest = Vector(0,0),
		viewPos = Vector(0,0),
		offs = Vector(0,0),
		top = 0,
		bottom = 640,
		speed = 900,
	}
end

function resetCamera()
	camera.pos = player.pos:copy()
	camera.dest = player.pos:copy()
	camera.offs.y = 0
end

function updateCamera(dt)
	local ss = Vector(400,640)
	local sh = ss * 0.5

	-- normal view
	if player.vel.y > 200 then camera.offs.y = ss.y*0.25 end
	if player.vel.y < -200 then camera.offs.y = -ss.y*0.25 end

	local sq = ss/6
	camera.dest = player.pos + camera.offs
	camera.dest = camera.dest:clamp(player.pos - sq, player.pos+sq)
	
	local dd = camera.dest - camera.pos
	if dd:mod() < camera.speed*dt then
		camera.pos = camera.dest:copy()
	else
		camera.pos = camera.pos + dd:norm() * camera.speed*dt
	end
	camera.pos = camera.pos:clamp(player.pos-sq, player.pos+sq)

	local maxPos = Vector(mapWidth, mapHeight + 200) - sh
	camera.pos = camera.pos:clamp(sh, maxPos)

	camera.viewPos.y = camera.pos.y - sh.y
	camera.viewPos.x = -(ss.x - mapWidth) * 0.5

	camera.top = camera.pos.y - sh.y
	camera.bottom = camera.pos.y + sh.y
end