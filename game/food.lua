Food = Food or {}

function initLostFood()
    Food.lostFood = {}
    Food.lostFoodDelay = 1.5

    -- pickups
    withImage("img/pickups.png",
        function(img)
            Food.img = img
            Food.foodBatch = love.graphics.newSpriteBatch(img, 2000)
            initFoodSheet()
        end)
    Food.foodList = {}
end

function resetLostFood()

    Food.lostFood = {}
end


function rememberFood(_ndx)
    table.insert(Food.foodList, {
        visTimer = 0,
        moveTimer = 0,
        posFraction = 0,
        ndx = _ndx,
        })
end

function prepareFoodAnim()
    -- 4 seconds total
    local animMinDuration = 4
    local foodTimeInc = math.max(0.01, animMinDuration/#Food.foodList)
    for i=1,#Food.foodList do
        Food.foodList[i].visTimer = foodTimeInc * i
    end

    setFoodDelay(foodTimeInc * #Food.foodList + 2.4)
end

function updateFoodBatch(dt)
    local foodSheet = getFoodSheet()
    
    Food.foodBatch:clear()

    if not isPlayerReturning() then return end

    local wifePos = getFamilyAnim(1).pos + Vector((mapWidth - 400) * 0.5, mapHeight - 380 + 10)

    local foodMoveDelay = 1
    for i=#Food.foodList,1,-1 do
        local food = Food.foodList[i]
        if food.visTimer > 0 then
            food.visTimer = food.visTimer - dt
            if food.visTimer <= 0 then food.fromPos = player.pos:copy() end
        elseif food.moveTimer < foodMoveDelay then
            food.moveTimer = food.moveTimer + dt
            food.posFraction = food.moveTimer/foodMoveDelay
            if food.moveTimer >= foodMoveDelay then
                -- remove from "anim" list
                table.remove(Food.foodList, i)

                -- sound
                playReceiveFoodSound()
                if #Food.foodList == 0 then
                    playLevelCompleteSound()
                end

                -- update food count
                foodCount = foodCount - 1
                startFoodAnim()
            end

            local foodPoint =  parabolePoint(food.fromPos, wifePos, food.posFraction)
            myAddBatch(Food.foodBatch, foodSheet[food.ndx], foodPoint.x, foodPoint.y,
                    0, 1, 1, 32, 32)
        end
    end
end

function loseFood(seedbias)
    if #Food.foodList <= 0 then return end
    math.randomseed(love.timer.getTime() + (seedbias or 0))
    local ang = (math.random() * 1.5 - 0.25) * math.pi 
    table.insert(Food.lostFood, {
        fromPos = player.pos:copy(),
        toPos = player.pos + VectorFromPolar(90, ang),
        ndx = Food.foodList[1].ndx,
        timer = 0,
        ang = 0,
        rotSpeed = (math.random() - 0.5) * 10
        })
    table.remove(Food.foodList,1)
    foodCount = foodCount - 1
    startFoodAnim()
    playFoodLeavingSound()
end

function getFoodSheet()
    return Food.sheet
end


function getFoodImage()
    return Food.img
end

function initFoodSheet()
    local img = getFoodImage()
    local pickupCount = 13
    Food.sheet = {}
    for i=1,4 do
        for j=0,3 do
            table.insert(Food.sheet,
                love.graphics.newQuad(j*64,Food.img:getHeight() - i*64, 64, 64, img:getWidth(), img:getHeight()))
            if #Food.sheet >= pickupCount then break end
        end
    end
end


function updateLostFood(dt)
    local foodSheet = getFoodSheet()
    for i=#Food.lostFood,1,-1 do
        local food = Food.lostFood[i]
        food.timer = food.timer + dt
        if food.timer >= Food.lostFoodDelay then
            creategoneAnim(food.toPos)
            table.remove(Food.lostFood, i)
            playFoodGoneSound()
        else
            food.ang = (food.ang + dt * food.rotSpeed) % (math.pi*2)
            local foodPoint = parabolePoint(food.fromPos, food.toPos, food.timer/Food.lostFoodDelay, true)
            myAddBatch(Food.foodBatch, foodSheet[food.ndx], foodPoint.x, foodPoint.y,
                    food.ang, 1, 1, 32, 32)
        end
    end
end
function drawLostFood()
    love.graphics.setColor(255,255,255)
    myDraw(Food.foodBatch)
end