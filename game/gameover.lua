function initGameOver()
    GameOver = {}

    withImage("img/gameover_bg.png", function(img) GameOver.bg = img end)
    withImage("img/gameover_bear.png", function(img) GameOver.bear = img end)

    GameOver.displayOpacity = 0
    GameOver.displaySpeed = 1
    GameOver.timer = 0
    GameOver.delay = 0.076*16
    GameOver.bearScale = 100
    GameOver.animOpacity = 0
    loadGameOverStencil()
end

function resetGameOver()
    GameOver.timer = 0
    GameOver.bearScale = 100
    GameOver.animOpacity = 0
    GameOver.displayOpacity = 0
end

function gameOverDone()
    return GameOver.displayOpacity == 1
end


function launchGameOver()
    GameOver.animOpacity = 0
    GameOver.bearScale = 100
    GameOver.displayOpacity = 0
    GameOver.timer = GameOver.delay
    pauseTrackr()
    playGameOverSound()
end

function isGameOverDone()
    return GameOver.bearScale <= 0
end


function updateGameOver(dt)
    if GameOver.timer > 0 then
        GameOver.timer = GameOver.timer - dt
        local frac = GameOver.timer / GameOver.delay
        GameOver.bearScale = math.pow(frac,3)*5
        GameOver.animOpacity = math.min((1-frac)*4, 1)
    end

    if GameOver.bearScale <= 0 then
        GameOver.displayOpacity = math.min(1, GameOver.displayOpacity + dt * GameOver.displaySpeed)
    end
end


function loadGameOverStencil()
    local shaderCode = [[
        vec4 effect ( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
        {
            if (Texel(texture, texture_coords).rgb == vec3(0.0))
                discard;
            return vec4(1.0);
        }
    ]]
    GameOver.shader = love.graphics.newShader(shaderCode)
    GameOver.stencil = function()
        love.graphics.setShader(GameOver.shader)
        love.graphics.draw(GameOver.bear, 200, 320,
            0, GameOver.bearScale, GameOver.bearScale,
            GameOver.bear:getWidth()*0.5, GameOver.bear:getHeight()*0.5)
        love.graphics.setShader()
    end
end

function drawGameOver()
    if islandIsGameOver() then
        love.graphics.setColor(255,255,255, GameOver.animOpacity*255)
        love.graphics.stencil(GameOver.stencil, "replace", 1)
        love.graphics.setStencilTest("less", 1)
        myDraw(GameOver.bg)
        love.graphics.setStencilTest()
        if GameOver.displayOpacity > 0 then
            love.graphics.setColor(255,255,255, GameOver.displayOpacity*255)
            foodAccumulated = math.min(9999, foodAccumulated)

            -- total food
            local cyphers = 1
            if foodAccumulated > 0 then cyphers = math.ceil(math.log(foodAccumulated+0.5)/math.log(10)) end

            local dispTxt = "x"..foodAccumulated
            myDraw(foodimg, 154 - cyphers*11, 240)
            drawNumber(dispTxt, 224 - cyphers*11 , 272, {255,255,255,GameOver.displayOpacity*255})

            -- total wheels
            local cyphers = 1
            if currentLevel > 0 then cyphers = math.ceil(math.log(currentLevel+0.5)/math.log(10)) end

            local dispTxt = "x"..currentLevel
            local will_ar = 400/640
            local real_ar = love.graphics.getWidth()/love.graphics.getHeight()

            local wheelImg = getWheelImage()
            myDraw(wheelImg, 190 - cyphers*11, 362, -math.pi/4, 0.25 * will_ar/real_ar, 0.25, wheelImg:getWidth()*0.5, wheelImg:getHeight()*0.5)
            drawNumber(dispTxt, 224 - cyphers*11 , 366, {255,255,255,GameOver.displayOpacity*255})
        end
    end
end