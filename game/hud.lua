function initHUD()
    withImage("img/food_pile.png",
        function(img) foodimg = img end)

    withImage("img/dialog_bubble.png",
        function(img) bubble_big_img = img end)

    withImage("img/bubble_round.png",
        function(img) bubble_small_img = img end)

    withImage("img/food_counter_bg.png",
        function(img) food_counter_img = img end)

    rectsCache = {}

    resetHUD()
end

function resetHUD()
    currentLevel = 1
    foodCount = 0
    foodAccumulated = 0
    resetFoodGoal()
    extraScale = 0
end

function resetFoodGoal()
    foodGoal = math.floor(10 * math.pow(1.26,currentLevel * 0.85))
    foodCount = 0
end

function reachedFoodGoal()
    return foodCount >= foodGoal
end

function updateHUD(dt)
    if extraScale ~= 0 then
        extraScale = decreaseExponential(dt, extraScale, 0.82)
    end
end

function setFoodCount(newCount)
    foodCount = newCount
end

function startFoodAnim()
    extraScale = 0.7
end

function incFoodCount_wAnim(ndx)
    foodCount = foodCount + 1
    if foodCount == foodGoal then launchReminder() end
    rememberFood(ndx)
    foodAccumulated = foodAccumulated + 1
    startFoodAnim()
end

function drawBubbleGoal()
    local advfrac = math.min((mapHeight - camera.viewPos.y - 400)/500, 1)
    local y = camera.viewPos.y + 280 + 200 * advfrac
    love.graphics.setColor(255,255,255, goalOpacity()*255)
    myDraw(bubble_big_img, -10, y-10)
    local cyphers = 1
    if foodGoal > 0 then cyphers = math.ceil(math.log(foodGoal+0.5)/math.log(10)) end

    local dispTxt = "x"..foodGoal.."!"
    myDraw(foodimg, 20+(3-cyphers)*6, y + 20)
    drawNumber(dispTxt, 84 + (3-cyphers)*12, y + 52, {128,128,128,goalOpacity()*255})
end

function drawHUD()
    local r,g,b = unpack(getSeasonColor())
    love.graphics.setColor(r,g,b,140)
    local dispNumber = math.min(999, foodCount)
    local dispTxt = "x "..dispNumber
    local cyphers = 1
    if foodCount > 0 then cyphers = math.ceil(math.log(dispNumber+0.5)/math.log(10)) end

    local xscale = (134 + cyphers * 20) / food_counter_img:getWidth()
    myDraw(food_counter_img, 190, 640 - 87, 0, xscale, 1)

    love.graphics.setColor(255,255,255)
    myDraw(foodimg, 200, 640 - 82)
    drawNumber(dispTxt, 280, 640-50, getSeasonLetterColor(), 1+extraScale)
end
