il = il or {}

function initIsland()
    withImage("img/island3.png",
        function(img) il.islandPic = img end)

   il.anims = {}
    local delays = {
        0.3,
        0.27,
        0.31,
        0.33
    }

    local positions = {
        Vector(124,352),
        Vector(69,450),
        Vector(118,427),
        Vector(87,404),
    }

    for i=1,4 do
        table.insert(il.anims, {
            frameNdx = 1,
            frameTime = 0,
            frameDelay = delays[i],
            pos = positions[i],
            size = Vector(64,64),
        })
    end

    il.anims[1].size = Vector(128,128)

    il.goalStates = {
        invisible = 1,
        appear = 2,
        visible = 3,
        fading = 4,
        gone = 5
    }

    il.goalTimes = {
        1, -- invisible = 1,
        0.5, -- appear = 0.5,
        6, -- visible = 10,
        2, -- fading = 2,
        0, -- gone = 0
    }

    il.islandStates = {
        begin = 1,
        playing = 2,
        returning = 3,
        gameover = 4
    }

    withImage("img/family_2.png",
        function(img)
            il.familyPic = img
            makeIslandBatch()
        end)
end


function resetIsland()
    for i=1,#il.anims do
        il.anims[i].frameNdx = 1
        il.anims[i].frameTime = 0
    end

    il.goal = {
        opacity = 1,
        state = il.goalStates.invisible,
        timer = 0,
        delay = il.goalTimes[1]
    }
    il.islandState = il.islandStates.begin
end

function makeIslandBatch()
    il.familyBatch = love.graphics.newSpriteBatch(il.familyPic, 100)
    il.familySprites = {}
    -- wife
    il.familySprites[1] = {}
    for x=0,2 do
        table.insert(il.familySprites[1],
            love.graphics.newQuad(x*128,0, 128, 128, il.familyPic:getWidth(), il.familyPic:getHeight()))
    end

    -- kids
    for y=0,2 do
        il.familySprites[y+2] = {}
        for x=0,3 do
            table.insert(il.familySprites[y+2],
               love.graphics.newQuad(x*64,(y+2)*64, 64, 64, il.familyPic:getWidth(), il.familyPic:getHeight()))
        end
    end
end

function updateFamilyBatch(dt)
    il.familyBatch:clear()
    for i=1,#il.anims do
        -- update anim
        local an = il.anims[i]
        local animlen = #il.familySprites[i]
        an.frameTime = an.frameTime + dt
        while an.frameTime > an.frameDelay do
            an.frameTime = an.frameTime - an.frameDelay
            an.frameNdx = (an.frameNdx % animlen)+1
        end
        -- prepare anim
        if an.pos.y < camera.bottom + 64 then
            myAddBatch(il.familyBatch, il.familySprites[i][an.frameNdx], an.pos.x, an.pos.y, 0, 1, 1, an.size.x*0.5, an.size.y*0.5)
        end
    end
end

function updateBubble(dt)
    if il.goal.state == il.goalStates.gone then return end

    if not checkInIsland(player.pos) and il.goal.state <= il.goalStates.visible then
        il.goal.state = il.goalStates.fading
        il.goal.delay = il.goalTimes[il.goal.state]
        il.goal.timer = 0
    end
    
    il.goal.timer = il.goal.timer + dt
    if il.goal.timer >= il.goal.delay then
        il.goal.timer = 0
        il.goal.state = il.goal.state + 1
        il.goal.delay = il.goalTimes[il.goal.state]
    end
    -- opacity depending on state
    if il.goal.state == il.goalStates.invisible then
        il.goal.opacity = 0
    elseif il.goal.state == il.goalStates.appear then
        il.goal.opacity = math.min(il.goal.timer / il.goal.delay, 1)
    elseif il.goal.state == il.goalStates.visible then
        il.goal.opacity = 1
    elseif il.goal.state == il.goalStates.fading then
        il.goal.opacity = math.max(1 - il.goal.timer / il.goal.delay, 0)
    elseif il.goal.state == il.goalStates.gone then
        il.goal.opacity = 0
    end
end

function updateIsland(dt)
    updateIslandState(dt)
    updateFamilyBatch(dt)
    updateBubble(dt)
    
end

function setFoodDelay(delay)
    il.foodDelay = delay
end


function updateIslandState(dt)
    if il.islandState == il.islandStates.begin and not checkInIsland(player.pos) then
        il.islandState = il.islandStates.playing
    elseif il.islandState == il.islandStates.playing and checkInIsland(player.pos) then
        if reachedFoodGoal() then
            prepareFoodAnim()
            il.foodTimer = 0
            il.islandState = il.islandStates.returning
            playReturningSound()
        else
            il.islandState = il.islandStates.begin
            il.goal.state = il.goalStates.invisible
        end
    elseif il.islandState == il.islandStates.returning then
        -- "eat" the food
        -- then start new level
        il.foodTimer = il.foodTimer+dt
        if il.foodDelay - il.foodTimer <= Transitions.preDelay and transitionsReady() then
            launchTransition(true)
        end
        if il.foodTimer >= il.foodDelay then
            increaseLevel()
        end

    elseif il.islandState == il.islandStates.gameover then
        -- done somewhere else
    end
end

function islandRequestStop()
    return il.islandState == il.islandStates.returning
end

function finishGame()
    storeScore(foodAccumulated, currentLevel)
    il.islandState = il.islandStates.gameover
    resetSeasonToFirst()
    resetMapMelt()
    launchGameOver()
end


function goalOpacity()
    return il.goal.opacity
end

function islandIsGameOver()
    return il.islandState == il.islandStates.gameover
end

function getFamilyAnim(memberNdx)
    return il.anims[memberNdx]
end

function getFamilyPic()
    return il.familyPic
end

function getFamilySprite(memberNdx, frameNdx)
    return il.familySprites[memberNdx][frameNdx]
end

function isPlayerReturning()
    return il.islandState == il.islandStates.returning
end

function drawIsland()
    love.graphics.setColor(255,255,255)
    myDraw(il.islandPic,(mapWidth - 400) * 0.5, mapHeight - 380)
    myDraw(il.familyBatch, (mapWidth - 400) * 0.5, mapHeight - 380)

end