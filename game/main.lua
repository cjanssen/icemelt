function love.load()
	love.filesystem.load("utils.lua")()
	love.filesystem.load("vector.lua")()
	love.filesystem.load("food.lua")()
	love.filesystem.load("map.lua")()
	love.filesystem.load("player.lua")()
	love.filesystem.load("camera.lua")()
	love.filesystem.load("seasons.lua")()
	love.filesystem.load("pickupanims.lua")()
	love.filesystem.load("transitions.lua")()
	love.filesystem.load("numbers.lua")()
	love.filesystem.load("island.lua")()
	love.filesystem.load("reminder.lua")()
	love.filesystem.load("hud.lua")()
	love.filesystem.load("title.lua")()
	love.filesystem.load("gameover.lua")()
	love.filesystem.load("score.lua")()
	love.filesystem.load("sound.lua")()
	love.filesystem.load("trackr.lua")()

	local OperatingSystem = love.system.getOS()
	if OperatingSystem == "Android" or OperatingSystem == "iOS" then
		Mobile = true
		love.window.setFullscreen(true)
	end

	love.graphics.setDefaultFilter("nearest","nearest",1)

	initThreads()
	initPickupAnims()
	initMap()
	initPlayer()
	initCamera()
	initSeasons()
	initTransitions()
	initNumbers()
	initHUD()
	initIsland()
	initReminder()
	initLostFood()
	initTitle()
	initGameOver()
	initHighscore()
	loadTrackr()
	initSound()
	resetGame()

end

function resetGame()
	resetTitle()
	resetSeasons()
	resetPlayer()
	resetPickupAnims()
	resetTransitions()
	resetIsland()
	resetReminder()
	resetLostFood()
	resetHUD()
	resetGameOver()
	resetMap()
end

function startGame()
	unpauseTrackr()
	proceedTrackr()
	generateMap()
end

function love.update(dt)
	if dt > 0.15 then return end
	if titleActive then
		checkThreads(dt)
		updateTitle(dt)
		updateHighscoreView(dt)
		if Trackr.paused and allThreadsFinished() then
			unpauseTrackr()
		end
	else
		updateMap(dt)
		updatePlayer(dt)
		updateCamera(dt)
		updateIsland(dt)
    	updateReminder(dt)
    	updateFoodBatch(dt)
		updateLostFood(dt)
		updateSeason(dt)
		updatePickupAnims(dt)
		updateTransitions(dt)
		updateHUD(dt)
		updateGameOver(dt)
	end
	updateTrackr(dt)
end

function love.draw()
	gameHeight = 640
	love.graphics.push()
	love.graphics.scale(love.graphics.getWidth()/400, love.graphics.getHeight()/640)

	if titleActive then
		if not highscoreVisible then
			drawTitle()
			drawProgressBar()
		else
			drawHighscore()
		end
	else

		drawBg()

		love.graphics.push()
		love.graphics.translate(-math.floor(camera.viewPos.x), -math.floor(camera.viewPos.y))

			drawMap()
			drawIsland()
			drawLostFood()
			drawPickupAnims()
			drawPlayer()
			drawBubbleGoal()

		love.graphics.pop()

		drawFg()
		drawHUD()
		drawWheel()
		drawReminder()
		drawTransitions()

		drawGameOver()
	end


	drawSoundIcon()

	love.graphics.pop()
end

function love.keypressed(key)
	if key=="escape" then
		if not titleActive then
			resetGame()
		else
			love.event.push("quit")
		end
		return
	end

	if gameOverDone() then
		resetGame()
	elseif titleActive and allThreadsFinished() then
		hideTitle()
	end
end

function love.mousepressed(x,y)
	mousePos = Vector(x,y)
	if mousePos.x < 42 * love.graphics.getWidth()/400 and mousePos.y < 42 * love.graphics.getHeight()/640 then
		switchSoundActive()
		return
	end

	if gameOverDone() then
		resetGame()
	elseif titleActive and allThreadsFinished() then
		hideTitle()
	end


end

function love.mousereleased(x,y)
	if mousePos then
		playerPush(Vector(x,y) - mousePos)
	end
end