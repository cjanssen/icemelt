Map = Map or {}

function initMap()
	local mapdata = {
		width = 11,
        height = 480,
        tilewidth = 32,
        tileheight = 32,
    }
	-- read tiles from mapdata
	Map.width,Map.height = mapdata.width,mapdata.height
	Map.tilewidth,Map.tileheight = mapdata.tilewidth,mapdata.tileheight
	Map.totalWidth,Map.totalHeight = mapdata.width*mapdata.tilewidth, mapdata.height*mapdata.tileheight
	Map.indices = {}

	---- pickups
	withImage("img/pickups.png",
		function(img)
			Map.pickupBatch = love.graphics.newSpriteBatch(img, 2000)
		end)

	Map.mapSeed = love.timer.getTime()
	-- generateMap()


	-- prepare sprites
	withImage("img/ice_sheet.png",
		function(img)
			Map.sheet = {}
			for season=1,4 do
				Map.sheet[season] = {}
				for k=0,2 do
					for j=0,15 do
						table.insert(Map.sheet[season],
							love.graphics.newQuad(
								j*64, img:getHeight() - season*64*3 + k*64, 64, 64, img:getWidth(), img:getHeight() 
								))
					end
				end
				Map.sheet[season+4] = {}
				for k=0,2 do
					for j=16,21 do
						table.insert(Map.sheet[season+4],
							love.graphics.newQuad(
								j*64, img:getHeight() - season*64*3 + k*64, 64, 64, img:getWidth(), img:getHeight() 
								))
					end
				end
			end

			Map.batch = love.graphics.newSpriteBatch(img, 10000)
		end)

	Map.initialMeltndx = 7
	Map.meltndx = Map.initialMeltndx

	Map.solids = {
		[1] = true,
		[2] = false,
		[3] = false,
		[4] = false,
		[5] = false,
		[6] = false,
	}

	Map.randomArrayTimer = 0
	Map.randomArrayDelay = 0.1

	-- public Map info
	mapWidth = Map.totalWidth
	mapHeight = Map.totalHeight
end


function resetMap()
	Map.meltndx = Map.initialMeltndx
	Map.randomArrayTimer = 0
end


function makeMapBatches()
	local seasonNdx = getCurrentSeasonIndex()
	-- prepare a list of random numbers
	if not Map.randomArray or Map.redoRandomArray then
		if Map.randomArray then
			math.randomseed(Map.randomArray[#Map.randomArray]*10000)
		else
			Map.randomArray = {}
		end
		for i=1,300 do Map.randomArray[i] = math.random() end
	end

	-- prepare batch
	Map.batch:clear()

	local yfrom = math.clamp(1,math.floor(camera.top / Map.tileheight),Map.height)
	local yto = math.clamp(1,math.ceil(camera.bottom / Map.tileheight),Map.height)

	for iy=yfrom,yto do
		math.randomseed(iy*120)
		local last = ((iy*3) % (#Map.randomArray-3)) + 1

		for ix=1,Map.width do
			local ndx = Map.indices[ix][iy]
			if ndx > 1 and ndx < 6 then
				myAddBatch(Map.batch, Map.sheet[seasonNdx][math.random(#Map.sheet[seasonNdx])],  
					(ix-0.5)*Map.tilewidth, (iy-0.5)*Map.tileheight, 0, 1, 1, 32, 32)
			elseif ndx == 6 then
				if Map.meltndx > Map.initialMeltndx-1 then
					local tileNdx = math.clamp(1,math.ceil(Map.randomArray[last] * #Map.sheet[seasonNdx]), #Map.sheet[seasonNdx])
					-- Map.batch:add(Map.sheet[seasonNdx][tileNdx],
					-- 		(ix-0.5)*Map.tilewidth + (Map.randomArray[last+1]-0.5)*2,
					-- 		(iy-0.5)*Map.tileheight + (Map.randomArray[last+2]-0.5)*2, 
					-- 		0, 1, 1, 32, 32)
					myAddBatch(Map.batch, Map.sheet[seasonNdx][tileNdx],
							(ix-0.5)*Map.tilewidth,
							(iy-0.5)*Map.tileheight, 
							0, 1, 1, 32, 32)
					last = ((last+2) % (#Map.randomArray-3)) + 1

				elseif Map.meltndx ~= 0 then
						myAddBatch(Map.batch, Map.sheet[seasonNdx+4][Map.meltndx],
							(ix-0.5)*Map.tilewidth, (iy-0.5)*Map.tileheight, 0, 1, 1, 32, 32)
				end
			end
		end
	end
end

function makePickupBatches()
	local foodSheet = getFoodSheet()
	Map.pickupBatch:clear()
	for i=1,#Map.pickupPositions do
		if Map.pickupPositions[i].y < camera.top - Map.tileheight then break end
		if Map.pickupPositions[i].y < camera.bottom + Map.tileheight then
			myAddBatch(Map.pickupBatch, foodSheet[Map.pickupPositions[i].ndx], Map.pickupPositions[i].x, Map.pickupPositions[i].y,
				0, 1, 1, 32, 32)
		end
	end
end

function updateMap(dt)
	Map.redoRandomArray = false
	Map.randomArrayTimer = Map.randomArrayTimer - dt
	if Map.randomArrayTimer <= 0 then
		Map.randomArrayTimer = Map.randomArrayTimer + Map.randomArrayDelay
		Map.redoRandomArray = true
	end
end

function randomArrayOutdated()
	return Map.redoRandomArray
end

function generateMap()
	math.randomseed(Map.mapSeed)
	Map.mapSeed = Map.mapSeed + 1
	Map.pickupPositions = {}
	resetPickupAnims()


	for iy=1,Map.height do
		for ix=1,Map.width do
			Map.indices[ix] = Map.indices[ix] or {}
			Map.indices[ix][iy] = 1
		end
	end

	currentLevel = currentLevel or 1
	local pickupProb = math.min(1, math.pow(1.22,0.62*currentLevel) * 0.03)
	local pickupTaboo = {}

	local foodSheet = getFoodSheet()

	-- create paths
	for iters = 1,2 do
		local prob = 0.75
		if iters==2 then prob = 1 end
		local dir = Vector(0,-1)
		local pos = Vector(math.ceil(Map.width*0.5), Map.height)

		Map.indices[pos.x][pos.y] = 2

		while pos.y > 1 do
			pos = pos + dir
			pos.x = math.clamp(2, pos.x, Map.width-1)
			dir = VectorFromAngle(math.random(3)*math.pi*0.5 + math.pi*0.5)
			dir.y = -math.abs(dir.y)
			if math.random()<prob then
				Map.indices[pos.x][pos.y] = 2
			end

			if math.random()<pickupProb and not checkInIsland(Vector(pos.x, pos.y*32 + 32)) then
				local pickupPos = pos.y*Map.width + pos.x
				if not pickupTaboo[pickupPos] then
					table.insert(Map.pickupPositions, Vector(pos.x*32, pos.y*32 - 24, {ndx = math.random(#foodSheet)}))
					pickupTaboo[pickupPos] = true
				end
			end
		end
	end

	-- island: last rows
	for iy=Map.height-1,Map.height do
		for ix = 1,Map.width do
			Map.indices[ix][iy] = 2
		end
	end

	-- sort pickups by position
	table.sort(Map.pickupPositions, function(a,b) return a.y > b.y end)

	-- create surroundings
	for iy=1,Map.height do
		for ix = 1,Map.width do
			if Map.indices[ix][iy] == 2 then
				local val = 3
				for ik = ix+1,Map.width do
					if Map.indices[ik][iy] ~= 2 then
						Map.indices[ik][iy] = math.max(val, Map.indices[ik][iy])
						if val < 5 then val = val + 1 else break end
					end
				end
				local val = 3
				for ik = ix-1,1,-1 do
					if Map.indices[ik][iy] ~= 2 then
						Map.indices[ik][iy] = math.max(val, Map.indices[ik][iy])
						if val < 5 then val = val + 1 else break end
					end
				end
			end
		end
	end

end

function updateSeasonMap()
	local seasonNdx = getCurrentSeasonIndex()
	if seasonNdx ~= 1 then
		for ix=1,Map.width do
			for iy=1,Map.height do
				if Map.indices[ix][iy] == 6 then
					Map.indices[ix][iy] = 1
				elseif Map.indices[ix][iy] > 2 then
					Map.indices[ix][iy] = Map.indices[ix][iy] + 1
				end
			end
		end
	else
		generateMap()
		resetPlayer()
	end

	resetMapMelt()
end

function increaseLevel()
	currentLevel = currentLevel + 1
	resetFoodGoal()
	resetSeasons()
	updateSeasonMap()
	resetPlayer()
	resetIsland()
end

function meltingStarted()
	return Map.meltndx >= 0
end

function meltFinished()
	return Map.meltndx == 0
end

function decreaseMapMelt()
	updateMapMelt(Map.meltndx - 1)
end

function resetMapMelt()
	updateMapMelt(Map.initialMeltndx)
end

function updateMapMelt(newNdx)
	if Map.meltndx ~= newNdx then
		Map.meltndx = newNdx
	end
end

function removeMapMelt()
	for ix=1,Map.width do
		for iy=1,Map.height do
			if Map.indices[ix][iy] == 6 then
				Map.indices[ix][iy] = 1
			end
		end
	end
end


function checkMapiCollision(pos)
	return Map.solids[Map.indices[pos.x][pos.y]]
end

function checkMapCollision(pos)
	local ix,iy = math.floor(pos.x/Map.tilewidth)+1,math.floor(pos.y/Map.tileheight)+1
	ix,iy = math.clamp(1,ix,Map.width), math.clamp(1,iy,Map.height)

	return Map.solids[Map.indices[ix][iy]]
end

function checkInIsland(pos)
	return pos.y >= (mapHeight - 78)
end


function checkPickups(pos)
	for i=1,#Map.pickupPositions do
		if (pos - Map.pickupPositions[i]):mod() < 32 then
			createPickupAnim(Map.pickupPositions[i])
			incFoodCount_wAnim(Map.pickupPositions[i].ndx)
			table.remove(Map.pickupPositions, i)
			playPickupSound()
			return
		end
		if Map.pickupPositions[i].y < pos.y - 200 then break end
	end
end

function drawMap()
	love.graphics.setColor(255,255,255)
	makeMapBatches()
	makePickupBatches()
	myDraw(Map.batch)
	myDraw(Map.pickupBatch)
end
