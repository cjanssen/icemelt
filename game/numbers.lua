function initNumbers()
    
    local numberIds = {
        ["0"] = "img/numbers/number_0.png",
        ["1"] = "img/numbers/number_1.png",
        ["2"] = "img/numbers/number_2.png",
        ["3"] = "img/numbers/number_3.png",
        ["4"] = "img/numbers/number_4.png",
        ["5"] = "img/numbers/number_5.png",
        ["6"] = "img/numbers/number_6.png",
        ["7"] = "img/numbers/number_7.png",
        ["8"] = "img/numbers/number_8.png",
        ["9"] = "img/numbers/number_9.png",
        ["x"] = "img/numbers/number_x.png",
        ["!"] = "img/numbers/number_excl.png",
    }

    numberPics = {}
    for key,fname in pairs(numberIds) do
        withImage(fname, function(img) numberPics[key] = img end)
    end
end

local function internalDrawNumber(sequence, x, y, color, scale)
        sequence = tostring(sequence)
        -- sequence is a string that might contain 0's, spaces and x's
        for i = 1,#sequence do
            local img = numberPics[sequence:sub(i,i)]
            if img then
                love.graphics.setColor(unpack(color))
                local hw = img:getWidth()*0.5
                myDraw(img, x + hw, y, 0, scale, scale, hw, img:getHeight()*0.5)
                x = x + img:getWidth() + 2
            else
                x = x + 10
            end
        end
    end

function drawNumber_wHighlight(sequence_, x_, y_, color_, scale_)   
    scale = scale_ or 1
    if color_ then
        local lightColor = {153 + color_[1]*0.4, 153 + color_[2]*0.4, 153 + color_[3]*0.4}
        internalDrawNumber(sequence_, x_ - scale, y_ - scale, lightColor, scale)
    end

    color = color_ or {255,255,255}
    internalDrawNumber(sequence_, x_, y_, color, scale)
end

function drawNumber(sequence, x, y, color, scale)
    -- sequence is a string that might contain 0's, spaces and x's
    color = color or {255,255,255}
    scale = scale or 1
    internalDrawNumber(sequence, x, y, color, scale)
end

function getNumberWidth(sequence)
    local x = 0
    for i = 1,#sequence do
        local img = numberPics[sequence:sub(i,i)]
        if img then
            x = x + img:getWidth() + 2
        else
            x = x + 10
        end
    end
    return x
end
