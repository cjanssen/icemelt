function initPickupAnims()
	pickupAnims = {}

	withImage("img/pickups_poof.png", 
		function(img)
			pickupAnims.sheet = {}
			for i=0,17 do
				table.insert(pickupAnims.sheet,
					love.graphics.newQuad(i*64,0, 64, 64, img:getWidth(), img:getHeight()))				
			end
			pickupAnims.batch = love.graphics.newSpriteBatch(img, 2000)
		end)

	---- gone
	goneAnims = {}
	withImage("img/sheet_poofgone.png",
		function(img)
			goneAnims.sheet = {}
			for i=0,12 do
				table.insert(goneAnims.sheet,
					love.graphics.newQuad(i*64,0, 64, 64, img:getWidth(), img:getHeight()))				
			end
			goneAnims.batch = love.graphics.newSpriteBatch(img, 2000)
		end)
end

function resetPickupAnims()
	pickupAnims.list = {}
	goneAnims.list = {}
end

function createPickupAnim(pos)
	table.insert(pickupAnims.list, {
		currentFrame = 1,
		timer = 0,
		delay = 0.1,
		pos = pos
	})
end

function creategoneAnim(pos)
	table.insert(goneAnims.list, {
		currentFrame = 1,
		timer = 0,
		delay = 0.1,
		pos = pos
	})
end

function updatePickupAnims(dt)

	for i=#pickupAnims.list,1,-1 do
		local anim = pickupAnims.list[i]
		anim.timer = anim.timer + dt
		while anim.timer > anim.delay do
			anim.timer = anim.timer - anim.delay
			anim.currentFrame = anim.currentFrame + 1
		end

		if anim.currentFrame > #pickupAnims.sheet then
			table.remove(pickupAnims.list, i)
		end
	end

	for i=#goneAnims.list,1,-1 do
		local anim = goneAnims.list[i]
		anim.timer = anim.timer + dt
		while anim.timer > anim.delay do
			anim.timer = anim.timer - anim.delay
			anim.currentFrame = anim.currentFrame + 1
		end

		if anim.currentFrame > #goneAnims.sheet then
			table.remove(goneAnims.list, i)
		end
	end
end

function drawPickupAnims()
	love.graphics.setColor(255,255,255)
	if #pickupAnims.list > 0 then
		pickupAnims.batch:clear()
		for i=1,#pickupAnims.list do
			local anim = pickupAnims.list[i]
			myAddBatch(pickupAnims.batch, pickupAnims.sheet[anim.currentFrame], anim.pos.x, anim.pos.y, 0, 1, 1, 32, 32)
		end
		myDraw(pickupAnims.batch)
	end

	if #goneAnims.list > 0 then
		goneAnims.batch:clear()
		for i=1,#goneAnims.list do
			local anim = goneAnims.list[i]
			myAddBatch(goneAnims.batch, goneAnims.sheet[anim.currentFrame], anim.pos.x, anim.pos.y, 0, 1, 1, 32, 32)
		end
		myDraw(goneAnims.batch)
	end
end