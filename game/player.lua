function initPlayer()
	player = {
		pos = Vector(0,0),
		dir = Vector(0,0),
		vel = Vector(0,0),
		acc = 2000,
		inertia = 0.98,
		orientation = 0,
		size = Vector(28,8),
		drownDelay = 1.3,
	}
	makePlayerBatch()
end

function resetPlayer()
	player.pos = Vector( 200, mapHeight - 12 )
	player.drownTimer = player.drownDelay
	player.dir = Vector(0,0)
	player.vel = Vector(0,0)

	setFoodCount(0)
	resetCamera()
end

function makePlayerBatch()
	-- prepare sprites
	withImage("img/bear_sheet.png",
		function(img)
			player.sprites = {}

			for i=1,4 do
				player.sprites[i] = {}
				for j=0,7 do
					table.insert(player.sprites[i],
						love.graphics.newQuad(j*64, img:getHeight()-i*64, 64, 64, img:getWidth(), img:getHeight()))
				end
			end

			player.batch = love.graphics.newSpriteBatch(img, 10)
		end)


	player.anim = {
		currentAnim = 1,
		currentFrame = 1,
		timer = 0,
		delay = 0.1,
		wantAnim = 1
	}

	-- 1 west (east)
	-- 2 down
	-- 3 up
	-- 4 drown
end

function updatePlayer(dt)
	playerEvalKeys()

	-- mouse
	if mousePos then
		local diff = Vector(love.mouse.getX(),love.mouse.getY()) - mousePos
		if diff:mod() > 150 then
			playerPush(diff)
			mousePos = nil
		end
	end

	movePlayer(dt)
	checkPlayerInWater(dt)
	updatePlayerAnims(dt)
	updateDrown(dt)
	checkPickups(player.pos+Vector(0,8))
end

function updatePlayerAnims(dt)
	player.anim.timer = player.anim.timer + dt
	while player.anim.timer > player.anim.delay do
		player.anim.timer = player.anim.timer - player.anim.delay
		player.anim.currentFrame = (player.anim.currentFrame % 8) + 1
	end

	if player.inWater then
		player.anim.currentAnim = 4
	else
		player.anim.currentAnim = player.anim.wantAnim
	end

	if player.vel:mod() < 150 then
		player.anim.delay = 0.1
	else
		player.anim.delay = 0.03
	end
end

function updateDrown(dt)
	if islandIsGameOver() then
		return
	end
	
	if player.inWater then
		local seedbias = 1
		player.drownTimer = player.drownTimer - dt
		if #Food.foodList == 0 and player.drownTimer + dt >= player.drownDelay*0.25 and player.drownTimer <= player.drownDelay*0.25 then
			playSwimSound()
		end
		while player.drownTimer <= 0 do
			player.drownTimer = player.drownTimer + player.drownDelay
			loseFood(seedbias)
			seedbias = seedbias + 1
		end
	else
		player.drownTimer = player.drownDelay * 0.5
	end
end

function playerEvalKeys()
	local dir = Vector(0,0)
	if love.keyboard.isDown("left") then
		dir.x = -1
	end
	if love.keyboard.isDown("right") then
		dir.x = 1
	end
	if love.keyboard.isDown("up") then
		dir.y = -1
	end
	if love.keyboard.isDown("down") then
		dir.y = 1
	end

	player.dir = dir
	if player.dir:mod() == 1 then
		player.orientation = math.floor(dir:angle()/(math.pi*0.5))
	end
end

function playerPush(dir)
	if islandIsGameOver() then
		return
	end
	
	player.vel = player.vel + dir

	if dir.x > 0 then player.lookright = true else player.lookright = false end
	if math.abs(dir.x) > math.abs(dir.y) then player.anim.wantAnim = 1 else
		if dir.y < 0 then player.anim.wantAnim = 3 else player.anim.wantAnim = 2 end
	end
end

function movePlayer(dt)
	if islandIsGameOver() then
		player.dir = Vector(0,0)
	end

	player.vel = player.vel * math.pow(player.inertia, 60*dt) + player.acc * player.dir * dt
	local newPos = player.pos + player.vel * dt

	if not islandRequestStop() or checkInIsland(newPos) then
		player.pos = newPos
	end



	player.pos.x = math.clamp(player.size.x*0.5, player.pos.x, mapWidth - player.size.x*0.5)
	player.pos.y = math.clamp(player.size.y*0.5, player.pos.y, mapHeight - player.size.y*0.5)

end

function checkPlayerInWater(dt)
	if checkMapCollision(player.pos + Vector(0,32), player.size) then
		if not player.inWater then
			playFallInWaterSound()
		end
		player.inWater = true
		player.acc = 1500
		player.inertia = 0.7
	else
		player.inWater = false
		player.acc = 2000
		player.inertia = 0.98
	end
end


function drawPlayer()
	love.graphics.setColor(255,255,255)
	player.batch:clear()
	local mirror = 1
	if player.lookright then mirror = -1 end
	myAddBatch(player.batch, player.sprites[player.anim.currentAnim][player.anim.currentFrame], player.pos.x, player.pos.y, 0, mirror, 1, 32, 32)

	myDraw(player.batch)
end
