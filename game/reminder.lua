reminderStates = reminderStates or {}
reminder = reminder or {}

function initReminder()
    reminderStates = {
        available = 1,
        charge = 2,
        popup = 3,
        stay = 4,
        hide = 5
    }
end

function resetReminder()
    reminder = {
        state = reminderStates.available,
        timer = 0,
        pos = Vector(50,690),
        memberNdx = 1,
        chargeDelay = 0.6,
        popupDelay = 1.3,
        stayDelay = 0.01,
        hideDelay = 1.5,
        rndNdx = 1,
        toppos = 300,
    }
end


function launchReminder()
    reminder.pos.y = 690
    reminder.state = reminderStates.available
    if reminder.state == reminderStates.available then
        reminder.state = reminderStates.charge
        reminder.timer = 0
        math.randomseed(love.timer.getTime())
        reminder.memberNdx = math.random(4)
    end
end

function updateReminder(dt)
    reminder.timer = reminder.timer + dt
    if reminder.state == reminderStates.available then
        reminder.pos.y = 690
        return
    elseif reminder.state == reminderStates.charge then
        reminder.pos.y = 690
        if reminder.timer >= reminder.chargeDelay then
            reminder.timer = 0
            reminder.state = reminderStates.popup
            playReminderSound()

        end
    elseif reminder.state == reminderStates.popup then
        local factor = reminder.timer / reminder.popupDelay
        reminder.pos.y = (reminder.toppos - 690) *math.pow(factor,0.2) + 690
        if reminder.timer >= reminder.popupDelay then
            reminder.timer = 0
            reminder.state = reminderStates.stay
        end
    elseif reminder.state == reminderStates.stay then
        reminder.pos.y = reminder.toppos
        if reminder.timer >= reminder.stayDelay then
            reminder.timer = 0
            reminder.state = reminderStates.hide
        end
    elseif reminder.state == reminderStates.hide then
        local factor = 1 - reminder.timer / reminder.hideDelay
        reminder.pos.y = (reminder.toppos - 690) * math.pow(factor,0.4) + 690

        if reminder.timer >= reminder.hideDelay then
            reminder.timer = 0
            reminder.state = reminderStates.available
        end
    end
end

function drawReminder()
    if reminder.state == reminderStates.available then return end

    local x,y = reminder.pos:unpack()

    love.graphics.setColor(255,255,255)
    myDraw(bubble_small_img, x-10, y)

    local function hole()
        love.graphics.rectangle("fill",x,y+10,90,75)
    end

    love.graphics.stencil(hole, "replace", 1)
    love.graphics.setStencilTest("greater", 0)
        local an = getFamilyAnim(reminder.memberNdx)
        myDrawQuad(getFamilyPic(),
            getFamilySprite(reminder.memberNdx, an.frameNdx),
            x + 47, y + 50 - an.size.y*0.2, 0, 1, 1, an.size.x*0.5, an.size.y*0.5)

        -- love.graphics.setColor(255,0,0,132)
        -- love.graphics.rectangle("fill",0,0,400,640)

    love.graphics.setStencilTest()
end