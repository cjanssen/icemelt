function initHighscore()
	Highscore = {}
	highscoreVisible = false
	highscoreTimer = 2
	highscoreDelay = 10
	slots = 6
	lastScore = nil
	loadHighscore()
	withImage("img/hiscorebg.png",
        function(img) hiscoreImg = img end)

end

function storeScore(food, lvl)
	lastScore = {food = food, lvl = lvl}

	table.insert(Highscore, lastScore)
	table.sort(Highscore, function(a,b) return a.lvl > b.lvl end)
	table.sort(Highscore, function(a,b) return a.food > b.food end)

	while table.getn(Highscore)>slots do
		table.remove(Highscore,table.getn(Highscore))
	end

	saveHighscore()
end

function updateHighscoreView(dt)
	if getProgress() ~= 1 then return end
	highscoreTimer = highscoreTimer - dt
	if highscoreTimer <= 0 then
		highscoreTimer = highscoreTimer + highscoreDelay
	end
	highscoreVisible = #Highscore > 0 and highscoreTimer > highscoreDelay * 0.5
end

function drawHighscore()
	love.graphics.setColor(255,255,255)
	myDraw(hiscoreImg)

	local x,y,opacity = 50,310,1
	opacity = opacity or 1

	local wheelImg = getWheelImage()
	local will_ar = 400/640
	local real_ar = love.graphics.getWidth()/love.graphics.getHeight()
        
	for i,v in ipairs(Highscore) do
		love.graphics.setColor(255,255,255)
		local ly = y + (i-1)*50
		myDraw(foodimg, x+64, ly, 0, 0.75, 0.75, foodimg:getWidth()*0.5, foodimg:getHeight()*0.5)
    	myDraw(wheelImg, x+240, ly, -math.pi/4, 0.1875 * will_ar/real_ar, 0.1875, wheelImg:getWidth()*0.5, wheelImg:getHeight()*0.5)
    	drawNumber(i, x-16, ly, {121,137,173}, 1)
		drawNumber("x"..v.food, x+96, ly, {121,137,173}, 1)
		drawNumber("x"..v.lvl, x+262, ly, {121,137,173}, 1)
	end

end

function saveHighscore()
	local st = "Highscore={"
	for i,v in ipairs(Highscore) do
		st = st.."{food="..v.food..",lvl="..v.lvl.."}"
		if i<table.getn(Highscore) then
			st = st..","
		end
	end
	st = st.."}"
	pcall(function() love.filesystem.write("scores.dat",st,string.len(st)) end)
end

function loadHighscore()
	local dta = love.filesystem.load("scores.dat")
	if dta then 
		pcall(dta) 
		if table.getn(Highscore)>0 then
		table.sort(Highscore, function(a,b) return a.lvl > b.lvl end)
		table.sort(Highscore, function(a,b) return a.food > b.food end)

		while table.getn(Highscore)>slots do
			table.remove(Highscore,table.getn(Highscore))
		end
	end
	end
end