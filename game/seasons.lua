Seasons = Seasons or {}

function initSeasons()
	Seasons.background_colors = {
		{199,203,208},
		{173,223,207},
		{189,177,247},
		{235,177,83},
	}

	Seasons.foreground_colors = {
		{156, 166, 173},
		{172, 205, 200},
		{178, 189, 233},
		{223, 160, 106},
	}

	Seasons.letter_colors = {
        {222, 225, 228},
        {227, 238, 237},
        {229, 233, 248},
        {244, 223, 205},
    }

    Seasons.foregrounds = {}
    withImage("img/vignette_winter_01.png", function(img) Seasons.foregrounds[1] = img end)
    withImage("img/vignette_spring_01.png", function(img) Seasons.foregrounds[2] = img end)
    withImage("img/vignette_summer_01.png", function(img) Seasons.foregrounds[3] = img end)
    withImage("img/vignette_autumn_01.png", function(img) Seasons.foregrounds[4] = img end)

    withImage("img/seasons_wheel.png", function(img) Seasons.wheelImg = img end)
	resetSeasons()
end

function resetSeasons()
	Seasons.currentSeason = 1
	Seasons.nextSeason = 2
	Seasons.seasonDelay = 27.5
	Seasons.seasonTimer = Seasons.seasonDelay
	Seasons.seasonMeltStart = 1
	Seasons.wheelAngle = 0
end

function resetSeasonToFirst()
    Seasons.currentSeason = 1
    Seasons.wheelAngle = -math.pi * 0.5 
end

function getSeasonColor()
	return Seasons.foreground_colors[Seasons.currentSeason]
end

function getSeasonLetterColor()
	return Seasons.letter_colors[Seasons.currentSeason]
end

function resetNextSeason()
	Seasons.nextSeason = 4
end

function setCurrentSeasonAsNext()
	Seasons.nextSeason = Seasons.currentSeason
end

function getCurrentSeasonIndex()
	return Seasons.currentSeason
end

function getNextSeasonIndex()
	return Seasons.nextSeason
end

function getWheelImage()
	return Seasons.wheelImg
end

function updateSeason(dt)
	if islandIsGameOver() then
		return
	end

	if not checkInIsland(player.pos) then
		Seasons.seasonTimer = Seasons.seasonTimer - dt
		if Seasons.seasonTimer <= Transitions.preDelay and transitionsReady() then
			launchTransition()
		end
	end


	if Seasons.seasonTimer <= 0 then
		Seasons.seasonTimer = Seasons.seasonTimer + Seasons.seasonDelay
		if Seasons.currentSeason < 4 then
			Seasons.currentSeason = Seasons.currentSeason + 1
			updateSeasonMap()
			if Seasons.currentSeason == 4 then
				-- autum start! remind user to come back home
				if not checkInIsland(player.pos) then 
					launchReminder() 
				end
			end
		else
			-- game over
			finishGame()
		end
	end

	local meltTime = Seasons.seasonDelay - Seasons.seasonTimer
	if randomArrayOutdated() and meltingStarted() and meltTime > Seasons.seasonMeltStart  then
		if meltFinished() then
			removeMapMelt()
		end
		decreaseMapMelt()
	end

	Seasons.wheelAngle = -(Seasons.currentSeason - (Seasons.seasonTimer / Seasons.seasonDelay)) * math.pi * 0.5 
end

function drawBg()
	love.graphics.setBackgroundColor(unpack(Seasons.background_colors[Seasons.currentSeason]))
end

function drawFg()
	love.graphics.setColor(255,255,255)
	local img = Seasons.foregrounds[Seasons.currentSeason]
	myDraw(img, 0, gameHeight, 0, 1, 1, 0, img:getHeight())
end

function drawWheel()
	love.graphics.setColor(255,255,255)

	local will_ar = 400/640
	local real_ar = love.graphics.getWidth()/love.graphics.getHeight()

	local img = Seasons.wheelImg
	myDraw(img, 0, gameHeight, Seasons.wheelAngle, will_ar/real_ar, 1, img:getWidth()*0.5, img:getHeight()*0.5)
end
