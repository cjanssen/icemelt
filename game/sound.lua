function initSound()
    Sounds = { active = true }

    withImage("img/snd_on_icon.png", function(img) Sounds.on_icon = img end)
    withImage("img/snd_off_icon.png", function(img) Sounds.off_icon = img end)

    local function addSnd(key) 
        return function(snd) Sounds[key] = snd  end
    end

    withSound("snd/pickup.ogg", addSnd("pickup"))
    withSound("snd/foodLeaving.ogg", addSnd("foodLeaving"))
    withSound("snd/foodGone.ogg", addSnd("foodGone"))
    withSound("snd/reminder.ogg", addSnd("reminder"))
    withSound("snd/season.ogg", addSnd("season"))
    withSound("snd/gameover.ogg", addSnd("gameover"))
    withSound("snd/waterFall.ogg", addSnd("waterFall"))
    withSound("snd/swim.ogg", addSnd("swim"))
    withSound("snd/receive.ogg", addSnd("receive"))
    withSound("snd/finished.ogg", addSnd("finished"))
    withSound("snd/returning.ogg", addSnd("returning"))

    loadSoundConfig()
end

function setSoundActive(active)
    Sounds.active = active
    switchTrackrEnabled(active)
    saveSoundConfig()
end

function switchSoundActive()
    setSoundActive(not Sounds.active)
end

function saveSoundConfig()
    local st = "return "..tostring(Sounds.active)
    pcall(function() love.filesystem.write("snd.dat",st,string.len(st)) end)
end

function loadSoundConfig()
    local dta = love.filesystem.load("snd.dat")
    if dta then 
        local shouldBeActive = Sounds.active
        pcall(function() shouldBeActive = dta()==true end)
        setSoundActive(shouldBeActive)
    end
end

function drawSoundIcon()
    if getProgress() == 1 then
        if Sounds.active then
            love.graphics.setColor(255,255,255)
            myDraw(Sounds.on_icon,10,10,0,0.5,0.5)
        else
            love.graphics.setColor(255,255,255,128)
            myDraw(Sounds.off_icon,10,10,0,0.5,0.5)
        end
    end
end


function playPickupSound()
    --ndcs = {22, 28, 8, 93},
    if Sounds.active then
        Sounds.pickup:rewind()
        Sounds.pickup:play()
    end
end


function playFoodLeavingSound()
    --{ 23, 25, 26, 28, 33, 34, 34, 36, 37, 37, 38, 51, 54, 54, 55, 93, 93 }
    if Sounds.active then
        Sounds.foodLeaving:rewind()
        Sounds.foodLeaving:play()
    end
end


function playFoodGoneSound()
    -- ndcs = { 30, 50, 51, 52, 55, 60, 74, 88 },
    if Sounds.active then
        Sounds.foodGone:rewind()
        Sounds.foodGone:play()
    end
end

function playReminderSound()
    --{ 27, 34, 39, 89 },
    if Sounds.active then
        Sounds.reminder:rewind()
        Sounds.reminder:play()
    end
end

function playSeasonSound()
    --ndcs = {35, 50, 64, 67, 71, 81, 82, 84 },
    if Sounds.active then
        Sounds.season:rewind()
        Sounds.season:play()
    end
end

function playGameOverSound()
    -- ndcs = { 85, 86, 87 },
    if Sounds.active then
        Sounds.gameover:rewind()
        Sounds.gameover:play()
    end
end

function playFallInWaterSound()
    -- ndcs = { 31, 32, 49, 53, 60, 68 },
    if Sounds.active then
        Sounds.waterFall:rewind()
        Sounds.waterFall:play()
    end
end

function playSwimSound()
    -- ndcs = { 30, 32, 33, 37, 38, 51, 56, 59, 6, 60, 65, 88, },
    if Sounds.active then
        Sounds.swim:rewind()
        Sounds.swim:play()
    end
end

function playReturningSound()
     -- ndcs = { 29, 35, 36, 92, 93, 94, },
    if Sounds.active then
        Sounds.returning:rewind()
        Sounds.returning:play()
    end
end

function playReceiveFoodSound()
    if Sounds.active then
        Sounds.receive:rewind()
        Sounds.receive:play()
    end
end

function playLevelCompleteSound()
    if Sounds.active then
        Sounds.finished:rewind()
        Sounds.finished:play()
    end
end
