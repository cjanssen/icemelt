function initTitle()
    -- withImage("img/title.png",
    --     function(img) titleImg = img end)

    -- SPECIAL: title must be loaded immediately
    titleImg = love.graphics.newImage("img/title.png")

    progressBarOpacity = 1
end

function resetTitle()
    titleActive = true
    startTrackr()
    loopTrackr()
end

function updateTitle(dt)
    if getProgress() == 1 then
        progressBarOpacity = decreaseExponential(dt, progressBarOpacity, 0.95)
    end
end

function hideTitle()
    titleActive = false
    startGame()
end

function drawTitle()
    love.graphics.setColor(255,255,255)
    myDraw(titleImg)
end

function drawProgressBar()
    if progressBarOpacity > 0 then
        love.graphics.setColor(0,0,0,32*progressBarOpacity)
        love.graphics.setLineWidth(2)
        local x,y = 70,560
        local w,h = 260,20
        love.graphics.rectangle("line",x,y,w,h)
        love.graphics.rectangle("fill",x,y,w*getProgress(),h)
    end
end
