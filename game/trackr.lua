
function loadTrackr()
    Trackr = {
        enabled = true,
        tracks = {
            love.audio.newSource("snd/tracks/drums.ogg","static"),
            love.audio.newSource("snd/tracks/melody.ogg","static"),
            love.audio.newSource("snd/tracks/acute.ogg","static"),
            love.audio.newSource("snd/tracks/bass.ogg","static"),
            love.audio.newSource("snd/tracks/hammer.ogg","static"),
            love.audio.newSource("snd/tracks/lead.ogg","static"),
            love.audio.newSource("snd/tracks/loopin.ogg","static"),
            love.audio.newSource("snd/tracks/whistles.ogg","static"),
        },
        volumes = {1,0,0,0,0,0,0,0},
        nextVol = {1,1,1,0,0,0,0,0},
        timer = 0,
        startTime = 0,
        loopTime = 16,
        paused = true,
        vol = 0.75,
        advancing = false,
        preDelay = 0,
    }

    Trackr.advOpts = love.audio.playSimultaneous ~= nil
    if Mobile then
        Trackr.preDelay = 0.005
    end

    if Trackr.advOpts then
        -- 2 seconds ahead, set next
        Trackr.preDelay = 2
    else
        love.audio.playSimultaneous = function(t)
            for _,track in ipairs(t) do track:play() end
        end
        love.audio.pauseSimultaneous = function(t)
            for _,track in ipairs(t) do track:pause() end
        end
    end
end

function startTrackr()
    Trackr.enabled = Sounds.active
    Trackr.volumes = {1,0,0,0,0,0,0,0}
    Trackr.nextVol = {1,1,1,0,0,0,0,0}
    Trackr.timer = Trackr.loopTime - Trackr.preDelay
    for ndx,track in ipairs(Trackr.tracks) do
        track:setLooping(true)
        track:setVolume(Trackr.volumes[ndx]*Trackr.vol)
        if Trackr.advOpts then
            track:setNextVolume(Trackr.volumes[ndx]*Trackr.vol)
        end
        track:rewind()
    end
end

function switchTrackrEnabled(enabled)
    Trackr.enabled = enabled
    if enabled and not Trackr.paused then
        love.audio.playSimultaneous(Trackr.tracks)
    else
        love.audio.pauseSimultaneous(Trackr.tracks)
    end

end

function loopTrackr()
    Trackr.advancing = false
end

function proceedTrackr()
    Trackr.advancing = true
end

function pauseTrackr()
    Trackr.paused = true
    love.audio.pauseSimultaneous(Trackr.tracks)
end

function unpauseTrackr()
    if Trackr.paused then
        Trackr.paused = false
        if Trackr.enabled then
            love.audio.playSimultaneous(Trackr.tracks)
        end
    end
end

function updateTrackr(dt)
    if Trackr.paused or not Trackr.enabled then return end
    Trackr.timer = Trackr.timer - dt

    if Trackr.timer <= 0 then
        Trackr.timer = Trackr.timer + Trackr.loopTime
        if Trackr.advancing then
            for ndx,track in ipairs(Trackr.tracks) do
                if Trackr.advOpts then
                    track:setNextVolume(Trackr.nextVol[ndx]*Trackr.vol)
                else
                    track:setVolume(Trackr.nextVol[ndx]*Trackr.vol)
                end
                Trackr.volumes[ndx] = Trackr.nextVol[ndx]
            end

            -- switch one-to-four tracks at random
            math.randomseed(os.clock())
            local ndxs = {1,2,3,4,5,6,7,8}
            local nswitch = 1 + math.random(3)
            for i=1,nswitch do
                local ndx = table.remove(ndxs,math.random(#ndxs))
                local mp,rp = 0.5 * (1 - 1/7), 0.5/7
                if Trackr.nextVol[ndx] == 0 or math.random()<=ndx*rp + mp then
                    Trackr.nextVol[ndx] = 1 - Trackr.nextVol[ndx]
                end
            end
            -- prevent silence
            local trackCount = 0
            for _,vol in ipairs(Trackr.nextVol) do
                if vol==1 then
                    trackCount = trackCount + 1
                end
            end

            -- special: last 2 tracks need heavier accompainment
            if Trackr.nextVol[7] == 1 and trackCount <= 4 then
                Trackr.nextVol[7] = 0
                trackCount = trackCount - 1
            end
            -- special: last 2 tracks need heavier accompainment
            if Trackr.nextVol[8] == 1 and trackCount <= 4 then
                Trackr.nextVol[8] = 0
                trackCount = trackCount - 1
            end

            if trackCount==0 then
                -- "-2" so that it's not any of the last 2 tracks
                local trackToActivate = math.random(#Trackr.nextVol-2)
                Trackr.nextVol[trackToActivate] = 1
            end
        end
    end
end
