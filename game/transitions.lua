function initTransitions()
	Transitions = {}

	withImage("img/transition_sheet.png",
		function(img)
			Transitions.sheet = {}
			for i=1,4 do
				Transitions.sheet[i] =
					love.graphics.newQuad(0,img:getHeight() - i*128, 128, 128, img:getWidth(), img:getHeight())
			end
			Transitions.batch = love.graphics.newSpriteBatch(img, 1000)
		end)

	Transitions.timer = 0
	Transitions.delay = 0.6
	Transitions.preDelay = Transitions.delay * 0.5

	-- shader
	loadTransitionShader()
end

function resetTransitions()
	Transitions.timer = 0
end

function launchTransition(fromWinter)
	Transitions.timer = Transitions.delay
	if fromWinter then
		resetNextSeason()
	else
		setCurrentSeasonAsNext()
	end
	prepareTransitionBatch()
	playSeasonSound()
end

function transitionsReady()
	return Transitions.timer <= 0
end

function loadTransitionShader()
	local shaderCode = [[
		uniform float radA;
		uniform float radB;
        vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
        {
        	vec2 sc = screen_coords / love_ScreenSize.x;
        	float rr = sqrt(dot(sc,sc));
        	if (rr<radA || rr>radB)
        		discard;
        	return vec4(1.0);
        }
    ]]

    Transitions.shader = love.graphics.newShader(shaderCode)
    Transitions.setRadius = function(rbig)
    	local rsmall = math.max(0, rbig-Transitions.rdiff)
	    Transitions.shader:send("radA",rsmall/400)
		Transitions.shader:send("radB",rbig/400)
   end
    Transitions.stencil = function()
    	love.graphics.setShader(Transitions.shader)
    	love.graphics.rectangle("fill",0,0,400,640)
    	love.graphics.setShader()
    end

	Transitions.rdiff = 575
	Transitions.maxRadius= math.sqrt(640*640+400*400) + Transitions.rdiff
end

function prepareTransitionTex()
	local cnv = love.graphics.newCanvas(16,16)
	love.graphics.setCanvas(cnv)
	love.graphics.setColor(255,255,255)
	love.graphics.rectangle("fill",-1,-1,18,18)
	love.graphics.setCanvas()
	return love.graphics.newImage(cnv:newImageData())
end

function prepareTransitionBatch()
	Transitions.batch:clear()
	-- local ndx = 2
	local ndx = getNextSeasonIndex()

	local Vx,Vy = Vector(50,-30),Vector(30,50)
	for i=-6,5 do
		for j=-1,11 do
			local pos = Vector(i*Vx.x+j*Vy.x, i*Vx.y+j*Vy.y)
			if pos.x + 128 > 0 and pos.x - 128 < 400 and pos.y + 128 > 0 and pos.y - 128 < 640 then
				myAddBatch(Transitions.batch, Transitions.sheet[ndx], pos.x, pos.y)
			end
		end
	end

	-- get the colors
	local tr_colors = {
		{{255,234,246},{255,213,231}}, -- spring
		{{252,236,20},{247,216,42}},  -- sum
		{{255,212,118},{255,188,122}}, -- aut
		{{112, 170, 215}, {174, 191, 215}}, -- win
	}

	-- make a mesh out of it
	if not Transitions.sqTex then
		Transitions.sqTex = prepareTransitionTex()
	end

	Transitions.bgMesh = love.graphics.newMesh({
		{0,  0,  0,0,  tr_colors[ndx][1][1], tr_colors[ndx][1][2], tr_colors[ndx][1][3] },
		{400,0,  1,0,  tr_colors[ndx][1][1], tr_colors[ndx][1][2], tr_colors[ndx][1][3] },
		{0,  640,0,1,  tr_colors[ndx][2][1], tr_colors[ndx][2][2], tr_colors[ndx][2][3] },
		{400,640,1,1,  tr_colors[ndx][2][1], tr_colors[ndx][2][2], tr_colors[ndx][2][3] },
		},
		"strip")
	Transitions.bgMesh:setTexture( Transitions.sqTex )

end

function updateTransitions(dt)
	if Transitions.timer > 0 then
		Transitions.timer = Transitions.timer - dt
		local frac = (Transitions.delay - Transitions.timer)/Transitions.delay
		Transitions.setRadius(frac*Transitions.maxRadius)
	end
end

function drawTransitions()
	if Transitions.timer>0 then
		love.graphics.stencil(Transitions.stencil, "replace", 1)
	    love.graphics.setStencilTest("greater", 0)
		love.graphics.setColor(255,255,255)
		myDraw(Transitions.bgMesh)
		myDraw(Transitions.batch)
		love.graphics.setStencilTest()
	end
end