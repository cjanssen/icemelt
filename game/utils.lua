function myDraw(img,x,y,a,sx,sy,cx,cy)
    x,y = x or 0,y or 0
    cx,cy = cx or 0,cy or 0
    love.graphics.draw(img,math.floor(x),math.floor(y),a,sx,sy,math.floor(cx),math.floor(cy))
end

function myDrawQuad(img,quad,x,y,a,sx,sy,cx,cy)
    x,y = x or 0,y or 0
    cx,cy = cx or 0,cy or 0
    love.graphics.draw(img,quad,math.floor(x),math.floor(y),a,sx,sy,math.floor(cx),math.floor(cy))
end

function myAddBatch(batch,quad,x,y,a,sx,sy,cx,cy)
    x,y = x or 0,y or 0
    cx,cy = cx or 0,cy or 0
    batch:add(quad,math.floor(x),math.floor(y),a,sx,sy,math.floor(cx),math.floor(cy))
end

function increaseExponential(dt, var, amount)
    if var < 1 then
        var = 1 - (1 - var) * math.pow(amount, 60*dt)
        if var > 0.999 then
            var = 1
        end
    end
    return var
end

function decreaseExponential(dt, var, amount)
    if var > 0 then
        var = var * math.pow(amount, 60*dt)
        if var < 0.001 then
            var = 0
        end
    end
    return var
end

function parabolePoint(bfrom, bto, frac, vertical)
   local bbetween = (bfrom + bto)/2
   local bortho = (bto - bfrom):ortho():norm()
   if bortho:angle() > 0 then bortho = -bortho end
   local bdist = math.max((bto-bfrom):mod() * 0.25, 30)
   if vertical then bdist = bdist * 3 end
   local p2 = bbetween + Vector(0, -bdist)
   if not vertical then p2 = p2 + bortho * bdist end

   local p2p = p2 - bfrom
   local p1p = bto - bfrom
   local K = 4 * p2p - p1p
   local R = 2 * p1p - 4 * p2p


   local point = bfrom + K * frac + R * frac * frac
   return point
end

-------------- threaded image read, but with coroutines instead
local Threads = {}

function initThreads()
  Threads.list = {}
  Threads.imageCount = 0
  Threads.co = coroutine.create(function()
      while #Threads.list>0 do
        local filename,func,isImg = unpack(table.remove(Threads.list,1))
        if isImg then
          local img = love.graphics.newImage(filename)
          coroutine.yield()
          func(img)
        else
          local snd = love.audio.newSource(filename)
          coroutine.yield()
          func(snd)
        end
        coroutine.yield()
      end
    end)
end

function withImage(filename, func)
  table.insert(Threads.list,{filename, func, true})
  Threads.imageCount = Threads.imageCount + 1
end

function withSound(filename, func)
  table.insert(Threads.list,{filename, func, false})
  Threads.imageCount = Threads.imageCount + 1
end

function checkThreads()
  if coroutine.status(Threads.co) == "suspended" then
    coroutine.resume(Threads.co)
  end
end

function getProgress()
  return (Threads.imageCount > 0) and 1-(#Threads.list / Threads.imageCount) or 1
end

function allThreadsFinished()
  return #Threads.list == 0
end
